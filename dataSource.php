<?php

class dataSource
{
	/**
	 * Database connection
	 *
	 * @var \PDO
	 */
    private static $databaseConnection;


	/**
	 * dataSource constructor.
	 */
    private function __construct() {

    }

	/**
	 * init dump
	 */
	public static function loadFixtures()
    {
        self::_createDatabaseConnection();
        $sql = file_get_contents('database.sql');
        self::$databaseConnection->exec($sql);
    }

	/**
	 * Return database connection object
	 *
	 * @return mixed
	 */
    public static function getDatabaseConnection() : \PDO
    {
    	if (self::$databaseConnection == null) {
		    self::_createDatabaseConnection();
	    }

        return self::$databaseConnection;
    }

	/**
	 * Create
	 */
    private static function _createDatabaseConnection()
    {
        self::$databaseConnection = new Pdo('sqlite:memory');
	    self::$databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
