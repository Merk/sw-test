<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 11.05.17 10:11
 */
class PaymentReport extends report {


	/**
	 * Getting report data
	 */
	public function createReport() {

		$db = dataSource::getDatabaseConnection();

		$query = 'SELECT COUNT(*) as ct, SUM(amount) as sm, strftime("%m.%Y", payments.create_ts) as mn '
		         .       'FROM payments JOIN documents ON payments.id NOT IN (documents.payment_id) '
		         .       'GROUP BY mn';

		$this->data = $db->query($query)->fetchAll();
	}

}